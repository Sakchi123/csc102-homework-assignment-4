# CSC102 Homework Assignment 4

Problem 1: 
   In the dictionary problem of the previous homework create an inheritance class to output the synonym and antonym along with meaning when the client inputs the word in the dictionary. Write the C++ code and algorithm of the problem (Marks 30)

Use the following .txt files
Synonym.txt

Word: foo; Synonym: A two o s’ file
Word: fooo; Synonym: A three o s’ file
Word: foooo; Synonym: A three o s’ file
Word: fooooo; Synonym: A four o s’ file
Word: foooooo; Synonym: A five o s’ file
Word: fooooooo; Synonym: A six o s’ file

Antonym.txt

Word: foo; Antonym: A short file
Word: fooo; Antonym: A short short file
Word: foooo; Antonym: A short short short file
Word: fooooo; Antonym: A short short short short file
Word: foooooo; Antonym: A short short short short short file
Word: fooooooo; Antonym: A short short short short short short file

 ---------------------------------------------------------------------------------------------------------------------------

Problem 2: 
Write a C++ program and algorithm for a class inheritance game where a kitten with random position in the gameboard is to be caught by its mother cat whose initial position is center to board and speed to particular direction a little higher than the kitten. The class member functions include a draw function where the mother cat stays at the middle. The direction is measured by the input function when say press ‘4’ means LEFT, ‘6’ means RIGHT, ‘8’ means UP,  ‘2’ means DOWN and ‘s’ means STOP. Use a score as the mother cat catches her kitten. Change the coordinates of the kitten and mother cat to zero, if they touch the wall.  (Marks 30)

Note: Use #include<conio.h> for console I/O and functions kbhit() which returns true if key board is pressed and getch() which returns ASCII value of the key pressed.

 ---------------------------------------------------------------------------------------------------------------------------

Problem 3: 
Write a C++ program and algorithm to measure the total surface area and volume of a cuboid made from a rectangle which is made from lines (length and breadth) and line finally made from points. The program should set values of point coordinates using constructor with parameter and finally output the total surface area and volume of the cuboid. (Marks 20)

 ---------------------------------------------------------------------------------------------------------------------------

Problem 4: 
Write a C++ program and algorithm inheriting the Ghost Game (Problem 2 of previous homework) to flourish the game using a randomly positioned Good Ghost which when present in door chosen by the player would add 100 to the player’s score and the game continues. (Marks 20)